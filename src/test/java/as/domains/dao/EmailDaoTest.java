package as.domains.dao;

import as.domains.data.Email;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class EmailDaoTest {
    private EmailDao emailDao;

    @Before
    public void setUp() {
        emailDao = new InMemoryEmailDao();
    }

    @Test
    public void whenNoElementsThenEmptyList() {
        List<Email> allEmails = emailDao.getAllEmails();
        assertThat(allEmails, empty());
    }

    @Test
    public void whenAddedOneElementThenOneElementRetrieved(){
        String email = "testemail@email.com";
        emailDao.addEmail(email);
        List<Email> allEmails = emailDao.getAllEmails();
        assertThat(allEmails, hasSize(1));
        assertThat(allEmails.get(0).getAddress(), is(email));
    }
}