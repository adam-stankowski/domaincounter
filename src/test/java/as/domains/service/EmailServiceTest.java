package as.domains.service;

import as.domains.dao.EmailDao;
import as.domains.data.Email;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

public class EmailServiceTest {

    private EmailDao emailDaoMock;
    private EmailService emailService;

    private final Email email = new Email("testemail@email.com");
    private final Email email2 = new Email("testemail2@email.com");
    private final Email email3 = new Email("testemail@yahoo.com");

    @Before
    public void setUp() {
        emailDaoMock = Mockito.mock(EmailDao.class);
        emailService = new EmailServiceImpl(emailDaoMock);
    }

    @Test
    public void whenNoEmailsAddedThenEmptyList() {
        Mockito.when(emailDaoMock.getAllEmails()).thenReturn(new ArrayList<>());
        assertThat(emailService.getAllEmails(), empty());
    }

    @Test
    public void whenEmailsReturnedByDaoThenReturnedByService() {
        Mockito.when(emailDaoMock.getAllEmails()).thenReturn(Arrays.asList(email, email2, email3));
        assertThat(emailService.getAllEmails(), hasSize(3));
        assertThat(emailService.getAllEmails(), containsInAnyOrder(email, email3, email2));
    }

    @Test
    public void whenEmailAddedThenRetrievedInGetAll() {
        emailService.addEmail(email.getAddress());
        Mockito.verify(emailDaoMock, times(1)).addEmail(email.getAddress());
        emailService.getAllEmails();
        Mockito.verify(emailDaoMock, times(1)).getAllEmails();
    }

    @Test
    public void whenIncorrectEmailPassedThenNoAction() {
        emailService.addEmail("wrongEmail");
        Mockito.verify(emailDaoMock, never()).addEmail(anyString());
        emailService.addEmail("wrongEmail@");
        Mockito.verify(emailDaoMock, never()).addEmail(anyString());
        emailService.addEmail("@email.com");
        Mockito.verify(emailDaoMock, never()).addEmail(anyString());
        emailService.addEmail("wrongEmail@.com");
        Mockito.verify(emailDaoMock, never()).addEmail(anyString());
    }
}