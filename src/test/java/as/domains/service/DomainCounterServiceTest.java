package as.domains.service;

import as.domains.data.Email;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class DomainCounterServiceTest {
    private DomainCounterService domainCounterService;
    private final Email email = new Email("testemail@email.com");
    private final Email email2 = new Email("testemail2@email.com");
    private final Email email3 = new Email("testemail@yahoo.com");

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        domainCounterService = new DomainCounterServiceImpl();
    }

    @Test
    public void whenZeroOrLessTopDomainsThenException() {
        expectedException.expect(IllegalArgumentException.class);
        domainCounterService.findMostFrequentEmailDomains(new ArrayList<>(), -1);
    }

    @Test
    public void whenNoEmailsThenEmptyOptional() {
        Optional<Map<String, Long>> domainStatistics = domainCounterService.findMostFrequentEmailDomains(new ArrayList<>(), 10);
        assertThat(domainStatistics.isPresent(), is(false));
    }

    @Test
    public void whenSingleEmailThenSingleStatistic() {
        Optional<Map<String, Long>> domainStatistics = domainCounterService.findMostFrequentEmailDomains(Arrays.asList(email), 10);
        assertThat(domainStatistics.isPresent(), is(true));
        assertThat(domainStatistics.map(stat -> stat.get("email.com")).isPresent(), is(true));
    }

    @Test
    public void whenTwoSameEmailsThenSingleStatisticWithTwoRecords() {
        Optional<Map<String, Long>> domainStatistics = domainCounterService.findMostFrequentEmailDomains(Arrays.asList(email, email2), 10);
        Long domainCount = domainStatistics.map(stat -> stat.get("email.com")).orElse(0L);
        assertThat(domainStatistics.isPresent(), is(true));
        assertThat(domainCount, is(2L));
    }

    @Test
    public void whenMoreDistinctDomainsThenStatisticsAreSorted() {

        Optional<Map<String, Long>> domainStatistics = domainCounterService.findMostFrequentEmailDomains(Arrays.asList(email3, email2, email), 10);
        Iterator<Map.Entry<String, Long>> iterator = domainStatistics.get().entrySet().iterator();
        Map.Entry<String, Long> firstElement = iterator.next();
        Map.Entry<String, Long> secondElement = iterator.next();

        assertThat(firstElement.getKey(), is("email.com"));
        assertThat(secondElement.getKey(), is("yahoo.com"));
    }

    @Test
    public void whenMoreThanTenDistinctDomainsThenMaxTenResults() {
        Optional<Map<String, Long>> domainStatistics = domainCounterService.findMostFrequentEmailDomains(generateRandomEmails(25), 10);
        int size = domainStatistics.get().size();
        assertThat(size, is(10));
    }

    private List<Email> generateRandomEmails(int howMany) {
        List<Email> result = new ArrayList<>();
        for (int i = 0; i < howMany; i++) {
            result.add(new Email(String.format("testemail@domain%d.com", i)));
        }
        return result;
    }
}