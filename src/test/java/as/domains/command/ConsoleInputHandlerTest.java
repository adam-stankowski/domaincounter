package as.domains.command;

import as.domains.service.DomainCounterService;
import as.domains.service.EmailService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.LinkedHashMap;
import java.util.Optional;
import java.util.function.Consumer;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.times;

public class ConsoleInputHandlerTest {
    private EmailService emailService;
    private DomainCounterService domainCounterService;
    private Consumer<String> consoleInputHandler;

    @Before
    public void setUp() {
        emailService = Mockito.mock(EmailService.class);
        domainCounterService = Mockito.mock(DomainCounterService.class);
        consoleInputHandler = new ConsoleInputHandler(emailService, domainCounterService);
    }

    @Test
    public void whenSlashThenDisplayStatistics() {
        Mockito.when(domainCounterService.findMostFrequentEmailDomains(anyList(), anyInt())).thenReturn(Optional.of(new LinkedHashMap<String, Integer>()));
        consoleInputHandler.accept("/");
        Mockito.verify(domainCounterService, times(1)).findMostFrequentEmailDomains(anyList(), anyInt());
    }

    @Test
    public void whenAnyOtherStringThenAddEmail() {
        consoleInputHandler.accept("anyText");
        Mockito.verify(emailService, times(1)).addEmail(anyString());
    }

}