package as.domains.service;

import as.domains.data.Email;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface DomainCounterService {
    Optional<Map<String, Long>> findMostFrequentEmailDomains(List<Email> emails, int howMany);
}
