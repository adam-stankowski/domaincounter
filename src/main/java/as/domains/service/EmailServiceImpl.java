package as.domains.service;

import as.domains.dao.EmailDao;
import as.domains.data.Email;

import java.util.List;
import java.util.regex.Pattern;

public class EmailServiceImpl implements EmailService {

    private EmailDao emailDao;
    private Pattern emailPattern;

    public EmailServiceImpl(EmailDao emailDao) {
        this.emailDao = emailDao;
        emailPattern = Pattern.compile("(.+?)@(.+?)\\.(.+?)");
    }

    @Override
    public void addEmail(String email) {
        if(isEmailCorrect(email)){
            emailDao.addEmail(email);
        }
    }

    private boolean isEmailCorrect(String email) {
        return emailPattern.matcher(email).matches();
    }

    @Override
    public List<Email> getAllEmails() {
        return emailDao.getAllEmails();
    }
}
