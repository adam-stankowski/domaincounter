package as.domains.service;

import as.domains.data.Email;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;

public class DomainCounterServiceImpl implements DomainCounterService {

    @Override
    public Optional<Map<String, Long>> findMostFrequentEmailDomains(List<Email> emails, int howMany) {
        if (howMany < 1) {
            throw new IllegalArgumentException();
        }

        if (emails.isEmpty()) {
            return Optional.empty();
        }

        Map<String, Long> domainCountMap = countDomainAppearance(emails);
        LinkedHashMap<String, Long> domainStatistics = findMostFrequentDomains(domainCountMap, howMany);
        return Optional.of(domainStatistics);
    }

    private Map<String, Long> countDomainAppearance(List<Email> emails) {
        return emails.stream()
                .collect(Collectors.groupingBy(Extractor::extractDomain, counting()));
    }

    private LinkedHashMap<String, Long> findMostFrequentDomains(Map<String, Long> domainCountMap, int howMany) {
        return domainCountMap.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(howMany)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }
}
