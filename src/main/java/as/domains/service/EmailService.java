package as.domains.service;

import as.domains.data.Email;

import java.util.List;

public interface EmailService {
    void addEmail(String email);
    List<Email> getAllEmails();
}
