package as.domains.service;

import as.domains.data.Email;

public class Extractor {
    public static String extractDomain(Email email) {
        String address = email.getAddress();
        int index = address.indexOf('@');
        return address.substring(index + 1);
    }
}
