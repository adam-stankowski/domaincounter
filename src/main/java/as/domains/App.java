package as.domains;

import as.domains.command.ConsoleInputHandler;
import as.domains.dao.EmailDao;
import as.domains.dao.InMemoryEmailDao;
import as.domains.service.DomainCounterService;
import as.domains.service.DomainCounterServiceImpl;
import as.domains.service.EmailService;
import as.domains.service.EmailServiceImpl;

import java.util.Scanner;
import java.util.function.Consumer;

public class App {
    public static void main(String[] args) {
        printUsage();

        EmailDao emailDao = new InMemoryEmailDao();
        EmailService emailService = new EmailServiceImpl(emailDao);
        DomainCounterService domainCounterService = new DomainCounterServiceImpl();
        Consumer<String> inputHandler = new ConsoleInputHandler(emailService, domainCounterService);
        try(Scanner scanner = new Scanner(System.in)) {
            while (scanner.hasNextLine()) {
                String input = scanner.nextLine();
                inputHandler.accept(input);
            }
        }
    }

    private static void printUsage() {
        System.out.println("Welcome to domain name counter");
        System.out.println("* To add an email, just type it here and press return");
        System.out.println("* To display email domain statistics type / and press return");
    }
}
