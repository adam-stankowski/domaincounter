package as.domains.dao;

import as.domains.data.Email;

import java.util.List;

public interface EmailDao {
    void addEmail(String email);
    List<Email> getAllEmails();
}
