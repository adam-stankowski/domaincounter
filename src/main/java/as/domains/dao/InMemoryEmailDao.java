package as.domains.dao;

import as.domains.data.Email;

import java.util.ArrayList;
import java.util.List;

public class InMemoryEmailDao implements EmailDao {
    private final List<Email> emails;

    public InMemoryEmailDao() {
        this.emails = new ArrayList<>();
    }

    @Override
    public void addEmail(String email) {
        emails.add(new Email(email));

    }

    @Override
    public List<Email> getAllEmails() {
        return new ArrayList<>(emails); //defensive copy
    }
}
