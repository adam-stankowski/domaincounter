package as.domains.command;

import as.domains.service.DomainCounterService;
import as.domains.service.EmailService;

import java.util.function.Consumer;

public class ConsoleInputHandler implements Consumer<String> {
    private EmailService emailService;
    private DomainCounterService domainCounterService;

    public ConsoleInputHandler(EmailService emailService, DomainCounterService domainCounterService) {
        this.emailService = emailService;
        this.domainCounterService = domainCounterService;
    }

    @Override
    public void accept(String input) {
        if("/".equals(input)){
            new PrintStatisticsCommand(domainCounterService).accept(emailService.getAllEmails());
        }
        else {
            new AddEmailCommand(emailService).accept(input);
        }
    }
}
