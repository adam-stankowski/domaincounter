package as.domains.command;

import as.domains.data.Email;
import as.domains.service.DomainCounterService;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class PrintStatisticsCommand implements Consumer<List<Email>> {
    private DomainCounterService domainCounterService;

    public PrintStatisticsCommand(DomainCounterService domainCounterService) {
        this.domainCounterService = domainCounterService;
    }

    @Override
    public void accept(List<Email> emails) {
        domainCounterService.findMostFrequentEmailDomains(emails, 10).ifPresent(PrintStatisticsCommand::printStatistics);
    }

    private static void printStatistics(Map<String, Long> emailStatisticsMap) {
        emailStatisticsMap.entrySet().forEach(PrintStatisticsCommand::printEntry);
    }

    private static void printEntry(Map.Entry<String, Long> stringLongEntry) {
        System.out.println(stringLongEntry.getKey() + " "+ stringLongEntry.getValue());
    }
}
