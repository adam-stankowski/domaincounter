package as.domains.command;

import as.domains.service.EmailService;

import java.util.function.Consumer;

public class AddEmailCommand implements Consumer<String> {
    private EmailService emailService;

    public AddEmailCommand(EmailService emailService) {
        this.emailService = emailService;
    }

    @Override
    public void accept(String s) {
        emailService.addEmail(s);
    }
}
